import java.awt.Point;

public class Move
{
	private Point savedPosition;
	private Direction savedDirection;

	public Move( Point pos, Direction dir )
	{
		savedPosition = pos;
		savedDirection = dir;
	}

	public Point getPosition()
	{
		return savedPosition;
	}

	public Direction getDirection()
	{
		return savedDirection;
	}
}
