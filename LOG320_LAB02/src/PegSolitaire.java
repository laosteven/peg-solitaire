import java.awt.Point;
import java.io.IOException;
import java.util.*;

public class PegSolitaire
{

	private static Point currentPosition;
	private static Stack<Move> history;
	private static Board gameBoard;

	public static void main( String[] args )
	{
		if (args != null && args.length != 0)
		{
			initializeBoard(args);
			gameBoard.printBoard(history.size());

			prompt();
		}
		else
		{
			System.out.println("Erreur: fichier non defini");
		}
	}

	private static void initializeBoard( String[] args )
	{
		try
		{
			gameBoard = new Board(args[0]);
		}
		catch (IOException e)
		{
			System.out.println("Erreur: " + e.getMessage());
		}
		currentPosition = gameBoard.getOpenPosition();
		history = new Stack<Move>();
	}

	private static void prompt()
	{
		startSolve();
//		boolean run = true;
//		while (run)
//		{
//			System.out.println("=======");
//			System.out.println("<1>:Solution <2>:Demarche <3>: Quitter");
//
//			Scanner sc = new Scanner(System.in);
//			int i = sc.nextInt();
//			switch (i)
//			{
//				case 1:
//					startSolve();
//					break;
//				case 2:
//					startShow();
//					break;
//				case 3:
//					run = false;
//					sc.close();
//					System.out.println("Fermeture");
//					break;
//				default:
//					System.out.println("Mauvaise valeur");
//			}
//		}
	}

	private static boolean startSolve()
	{
//		for(gameBoard.get)
		int move = 0;
		for(int i = 0; i < gameBoard.getSize(); i++)
		{
			for(int j = 0; j < gameBoard.getSize(); j++)
			{
				int tries = 0;
				for(Direction dir : Direction.values())
				{
					if(gameBoard.jump(gameBoard.getOpenPosition(), dir))
					{
						history.push(new Move(gameBoard.getOpenPosition(), dir));
						gameBoard.printBoard(move++);
					}
					else if(tries == Direction.values().length - 1)
					{
						try
						{
							Move last = history.pop();
							gameBoard.jumpBack(last);
						}
						catch(EmptyStackException e)
						{
							System.out.println("Historique vide");
							return false;
						}
					}
					tries++;
				}
			}
		}
		return false;
	}

	private static void startShow()
	{

	}
}
