import java.awt.Point;
import java.io.*;
import java.util.*;

public class Board
{
	private int[][] gameArray;
	private Point openPosition;

	private static final int OCCUPIED = 1;
    private static final int EMPTY = 2;
	
	public Board( String fileName ) throws IOException
	{
		// Extension check
		if (!fileName.toLowerCase().endsWith(".puzzle"))
		{
			throw new IOException("Fichier incompatible -- N'accepte seulement que .puzzle");
		}

		// File reading
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		List<String> tempList = new ArrayList<String>();
		String line;
		int tempLength = 0;
		while ((line = br.readLine()) != null)
		{
			// Populate array
			tempList.add(line);
			if (tempLength == 0 || tempLength == line.length())
			{
				tempLength = line.length();
			}
			else
			{
				throw new IOException("Format de tableau incorrect");
			}
		}
		System.out.println(fileName + " charge avec succes");
		br.close();

		// Array building
		int[][] tempArray = new int[tempLength][tempLength];
		int i = 0;
		for (String str : tempList)
		{
			int j = 0;
			for (char chr : str.toCharArray())
			{
				Integer val = Integer.valueOf(String.valueOf(chr));
				tempArray[i][j] = val;
				j++;
			}
			i++;
		}
		gameArray = tempArray;
	}

	public boolean jump( Point pos, Direction dir )
	{
		// Verifier si position suivante est soit vide ou occupee
		int xNewPos = pos.x + dir.getMoveX();
		int yNewPos = pos.y + dir.getMoveY();

		
		// Verifier que ca ne soit pas OutOfBounds
		boolean xCheck = xNewPos > 0 && xNewPos <= gameArray.length;
		boolean yCheck = yNewPos > 0 && yNewPos <= gameArray.length;
		if (!xCheck || !yCheck)
		{
			return false;
		}
		
		// Verifier qu'un pion existe
		int xBetween = pos.x + dir.getMoveX()/2;
		int yBetween = pos.y + dir.getMoveY()/2;
		if(gameArray[xBetween][yBetween] == OCCUPIED)
		{
			gameArray[pos.x][pos.y] = OCCUPIED;
			gameArray[xBetween][yBetween] = EMPTY;
			gameArray[xNewPos][yNewPos] = EMPTY;
			openPosition.move(xNewPos, yNewPos);
			return true;
		}
		return false;
	}

	public int getCenter()
	{
		return (gameArray.length + 1) / 2;
	}

	public Point getOpenPosition()
	{
		for(int i = 0; i < gameArray.length; i++)
		{
			for(int j = 0; j < gameArray.length; j++)
			{
				if(gameArray[i][j] == 2)
				{
					if(openPosition == null)
					{
						openPosition = new Point(i,j);
					}
					else if(new Point(i,j) != openPosition)
					{
						openPosition = new Point(i,j);
					}
					return openPosition;
				}
			}
		}
		return new Point();
	}

	public void printBoard( int currentMove )
	{
		System.out.println("=======");
		System.out.println("Move #" + currentMove);
		System.out.println("-------");
		StringBuilder str = new StringBuilder();
		for (int i = 0; i < gameArray.length; i++)
		{
			for (int j = 0; j < gameArray.length; j++)
			{
				str.append(gameArray[i][j]);
			}
			System.out.println(str);
			str = new StringBuilder();
		}
	}

	public int getSize()
	{
		return gameArray.length;
	}

	public void jumpBack(Move last) 
	{
		// Retour arriere
		int xBetween = openPosition.x - last.getDirection().getMoveX() / 2;
		int yBetween = openPosition.y - last.getDirection().getMoveY() / 2;
		gameArray[openPosition.x][openPosition.y] = 1;
		gameArray[xBetween][yBetween] = 1;
		openPosition = last.getPosition();
		gameArray[openPosition.x][openPosition.y] = 2;
		
	}
}
