
public enum Direction 
{
	// y, x
	HAUT(-2,0),
	DROITE(0,2),
	BAS(2,0),
	GAUCHE(0,-2);
	
	private int moveX;
	private int moveY;
	
	Direction(int x, int y)
	{
		this.moveX = x;
		this.moveY = y;
	}

	public int getMoveY() {
		return this.moveY;
	}

	public int getMoveX() {
		return this.moveX;
	}
}
