# Backtracking Peg Solitaire #
-------

Solve a game of **[Peg Solitaire](https://en.wikipedia.org/wiki/Peg_solitaire)** using a **[backtracking](https://en.wikipedia.org/wiki/Backtracking)** algorithm.

Once the algorithm manages to solve the puzzle, an output file in `txt` format will be generated, giving information about:

* step-by-step moves;
* the current gameboard;
* **explored nodes** to find the solution (basically, the amount of moves that were made while backtracking);
* solving time in milliseconds, which is used to measure **performance**.

The output follows this format:

```
#!
#1: (1, 3): bas
  0 1 2 3 4 5 6
  -------------
0|0 0 1 1 1 0 0 
1|0 0 1 1 1 0 0 
2|1 1 1 1 1 1 1 
3|1 1 1 2 1 1 1 
4|1 1 1 1 1 1 1 
5|0 0 1 1 1 0 0 
6|0 0 1 1 1 0 0 
================
...
================
#32: Temps: 141 ms | Noeuds: 20275
  0 1 2 3 4 5 6
  -------------
0|0 0 2 2 2 0 0 
1|0 0 2 2 2 0 0 
2|2 2 2 2 2 2 2 
3|2 2 2 2 2 2 2 
4|2 2 2 2 2 2 2 
5|0 0 2 2 2 0 0 
6|0 0 2 1 2 0 0 
```

Users can create a custom board in a *puzzle* file format with the following rules:

* `0`: non-playable space
* `1`: peg
* `2`: empty space

For example, the traditional English board in `.puzzle` format will look like this:
```
#!
0 0 1 1 1 0 0 
0 0 1 1 1 0 0 
1 1 1 1 1 1 1 
1 1 1 2 1 1 1 
1 1 1 1 1 1 1 
0 0 1 1 1 0 0 
0 0 1 1 1 0 0 
```

# Objectives #
-------
* Familiarize with **backtracking** algorithm.
* Decide which strategy to implement to get the *best moves.*
* Play with the **direction** order to shorten solving time.

# How do I get set up? #
-------
* Download the runnable `.jar` file in the project's root folder.
* Save it in an easily accessible folder location.
* Open the command prompt and write: `java -jar peg-solitaire.jar <filename.puzzle>`

# For more information #
-------
Visit the following website: [**Data structure and Algorithms** (LOG320)](https://www.etsmtl.ca/Programmes-Etudes/1er-cycle/Fiche-de-cours?Sigle=log320) [*fr*]